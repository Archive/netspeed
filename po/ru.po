# translation of netspeed.master.ru.po to Russian
# Nail Abdrahmanov <nail@gella.com.ru>, 2002.
# Диконов Вячеслав (Vyacheslav Dikonov) <slava@altlinux.ru>, 2002.
# Yuri Kozlov <yuray@komyakino.ru>, 2010.
# Yuri Myasoedov <omerta13@yandex.ru.ru>, 2011.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
msgid ""
msgstr ""
"Project-Id-Version: netspeed.HEAD\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=netspeed&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2011-04-20 12:39+0000\n"
"PO-Revision-Date: 2011-05-14 12:50+0300\n"
"Last-Translator: Yuri Myasoedov <omerta13@yandex.ru>\n"
"Language-Team: Russian <gnome-cyr@gnome.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../GNOME_NetspeedApplet.server.in.in.h:1
msgid "Internet"
msgstr "Интернет"

#: ../GNOME_NetspeedApplet.server.in.in.h:2
msgid "Netspeed Applet"
msgstr "Апплет скорости сети"

#: ../GNOME_NetspeedApplet.server.in.in.h:3
msgid "Netspeed Applet Factory"
msgstr "Фабрика апплета скорости сети"

#: ../GNOME_NetspeedApplet.server.in.in.h:4
msgid "Network Monitor"
msgstr "Сетевой монитор"

#: ../src/netspeed.c:409
msgid "b/s"
msgstr "б/с"

#: ../src/netspeed.c:409
msgid "B/s"
msgstr "Б/с"

#: ../src/netspeed.c:411
msgid "bits"
msgstr "бит"

#: ../src/netspeed.c:411
msgid "bytes"
msgstr "байт"

#: ../src/netspeed.c:418
msgid "kb/s"
msgstr "кб/с"

#: ../src/netspeed.c:418
msgid "KiB/s"
msgstr "КиБ/с"

#: ../src/netspeed.c:420
msgid "kb"
msgstr "кб"

#: ../src/netspeed.c:420
msgid "KiB"
msgstr "КиБ"

#: ../src/netspeed.c:429
msgid "Mb/s"
msgstr "Мб/с"

#: ../src/netspeed.c:429
msgid "MiB/s"
msgstr "МиБ/с"

#: ../src/netspeed.c:431
msgid "Mb"
msgstr "Мб"

#: ../src/netspeed.c:431
msgid "MiB"
msgstr "МиБ"

#: ../src/netspeed.c:757
#, c-format
msgid ""
"There was an error displaying help:\n"
"%s"
msgstr ""
"Ошибка при показе справки:\n"
"%s"

#: ../src/netspeed.c:808
#, c-format
msgid ""
"Failed to show:\n"
"%s"
msgstr ""
"Не удалось показать:\n"
"%s"

#: ../src/netspeed.c:839
msgid "A little applet that displays some information on the traffic on the specified network device"
msgstr "Индикатор скорости показывает сведения о траффике, проходящем через выбранное сетевое устройство"

#: ../src/netspeed.c:842
msgid "translator-credits"
msgstr ""
"Вячеслав Диконов\n"
"Юрий Козлов <yuray@komyakino.ru>, 2010\n"
"Юрий Мясоедов <omerta13@yandex.ru>, 2011"

#: ../src/netspeed.c:844
msgid "Netspeed Website"
msgstr "Веб-страница апплета"

#: ../src/netspeed.c:970
msgid "Netspeed Preferences"
msgstr "Настройки"

#: ../src/netspeed.c:993
msgid "General Settings"
msgstr "Общие параметры"

#: ../src/netspeed.c:1014
msgid "Network _device:"
msgstr "Сетевое _устройство:"

#. Default means device with default route set
#: ../src/netspeed.c:1025
msgid "Default"
msgstr "По умолчанию"

#: ../src/netspeed.c:1038
msgid "Show _sum instead of in & out"
msgstr "Показывать суммарную скорость входящего и исходящего трафика"

#: ../src/netspeed.c:1042
msgid "Show _bits instead of bytes"
msgstr "Показывать скорость в _бит/сек (б/с) вместо байт/сек (Б/с)"

#: ../src/netspeed.c:1046
msgid "Change _icon according to the selected device"
msgstr "Менять _значок в соответствии с типом устройства"

#: ../src/netspeed.c:1157
#, c-format
msgid "Device Details for %s"
msgstr "Информация об устройстве %s"

#: ../src/netspeed.c:1183
msgid "_In graph color"
msgstr "Цвет графика _входящих данных"

#: ../src/netspeed.c:1184
msgid "_Out graph color"
msgstr "Цвет графика _исходящих данных"

#: ../src/netspeed.c:1200
msgid "Internet Address:"
msgstr "IP адрес:"

#: ../src/netspeed.c:1201
msgid "Netmask:"
msgstr "Маска сети:"

#: ../src/netspeed.c:1202
msgid "Hardware Address:"
msgstr "Аппаратный адрес:"

#: ../src/netspeed.c:1203
msgid "P-t-P Address:"
msgstr "Адрес точка-точка:"

#: ../src/netspeed.c:1204
msgid "Bytes in:"
msgstr "Объём входящего трафика:"

#: ../src/netspeed.c:1205
msgid "Bytes out:"
msgstr "Объём исходящего трафика:"

#: ../src/netspeed.c:1207
#: ../src/netspeed.c:1208
#: ../src/netspeed.c:1209
#: ../src/netspeed.c:1210
msgid "none"
msgstr "нет"

#: ../src/netspeed.c:1249
msgid "IPv6 Address:"
msgstr "Адрес IPv6:"

#: ../src/netspeed.c:1281
msgid "Signal Strength:"
msgstr "Мощность сигнала:"

#: ../src/netspeed.c:1282
msgid "ESSID:"
msgstr "ESSID:"

#: ../src/netspeed.c:1368
#, c-format
msgid "Do you want to disconnect %s now?"
msgstr "Отключить %s сейчас?"

#: ../src/netspeed.c:1372
#, c-format
msgid "Do you want to connect %s now?"
msgstr "Подключить %s сейчас?"

#: ../src/netspeed.c:1398
#, c-format
msgid ""
"<b>Running command %s failed</b>\n"
"%s"
msgstr ""
"<b>Сбой выполнения команды %s</b>\n"
"%s"

#: ../src/netspeed.c:1454
#, c-format
msgid "%s is down"
msgstr "%s отключен"

#: ../src/netspeed.c:1459
#, c-format
msgid ""
"%s: %s\n"
"in: %s out: %s"
msgstr ""
"%s :%s\n"
"входящий: %s исходящий: %s"

#: ../src/netspeed.c:1461
#: ../src/netspeed.c:1470
msgid "has no ip"
msgstr "нет IP адреса"

#: ../src/netspeed.c:1468
#, c-format
msgid ""
"%s: %s\n"
"sum: %s"
msgstr ""
"%s :%s\n"
"суммарная скорость: %s"

#: ../src/netspeed.c:1477
#, c-format
msgid ""
"\n"
"ESSID: %s\n"
"Strength: %d %%"
msgstr ""
"\n"
"ESSID: %s\n"
"Мощность: %d %%"

#: ../src/netspeed.c:1478
msgid "unknown"
msgstr "неизвестно"

#: ../src/netspeed.c:1525
msgid "Netspeed"
msgstr "Индикатор скорости"

#: ../src/netspeed.c:1693
msgid "Device _Details"
msgstr "_Подробнее об устройстве"

#: ../src/netspeed.c:1693
msgid "_Preferences..."
msgstr "_Настройки..."

#: ../src/netspeed.c:1693
msgid "_Help"
msgstr "_Справка"

#: ../src/netspeed.c:1693
msgid "_About..."
msgstr "_Об апплете..."

