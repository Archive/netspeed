# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# Aviary.pl
# Jeśli masz jakiekolwiek uwagi odnoszące się do tłumaczenia lub chcesz
# pomóc w jego rozwijaniu i pielęgnowaniu, napisz do nas:
# gnomepl@aviary.pl
# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# Paweł Marciniak <pmarciniak@lodz.home.pl>, 2006.
# Julian Sikorski <belegdol@gmail.com>, 2008.
# Piotr Drąg <piotrdrag@gmail.com>, 2011-2012.
# Aviary.pl <gnomepl@aviary.pl>, 2011-2012.
msgid ""
msgstr ""
"Project-Id-Version: netspeed\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-09-04 20:01+0200\n"
"PO-Revision-Date: 2012-09-04 20:02+0200\n"
"Last-Translator: Piotr Drąg <piotrdrag@gmail.com>\n"
"Language-Team: Polish <gnomepl@aviary.pl>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Poedit-Language: Polish\n"
"X-Poedit-Country: Poland\n"

#: ../GNOME_NetspeedApplet.server.in.in.h:1
msgid "Netspeed Applet Factory"
msgstr "Generator apletu prędkości sieci"

#: ../GNOME_NetspeedApplet.server.in.in.h:2
msgid "Netspeed Applet"
msgstr "Aplet prędkości sieci"

#: ../GNOME_NetspeedApplet.server.in.in.h:3
msgid "Network Monitor"
msgstr "Monitor sieci"

#: ../GNOME_NetspeedApplet.server.in.in.h:4
msgid "Internet"
msgstr "Internet"

#: ../src/netspeed.c:409
msgid "b/s"
msgstr "b/s"

#: ../src/netspeed.c:409
msgid "B/s"
msgstr "B/s"

#: ../src/netspeed.c:411
msgid "bits"
msgstr "bitów"

#: ../src/netspeed.c:411
msgid "bytes"
msgstr "bajtów"

#: ../src/netspeed.c:418
msgid "kb/s"
msgstr "kb/s"

#: ../src/netspeed.c:418
msgid "KiB/s"
msgstr "KiB/s"

#: ../src/netspeed.c:420
msgid "kb"
msgstr "kb"

#: ../src/netspeed.c:420
msgid "KiB"
msgstr "KiB"

#: ../src/netspeed.c:429
msgid "Mb/s"
msgstr "Mb/s"

#: ../src/netspeed.c:429
msgid "MiB/s"
msgstr "MiB/s"

#: ../src/netspeed.c:431
msgid "Mb"
msgstr "Mb"

#: ../src/netspeed.c:431
msgid "MiB"
msgstr "MiB"

#: ../src/netspeed.c:757
#, c-format
msgid ""
"There was an error displaying help:\n"
"%s"
msgstr ""
"Wystąpił błąd podczas wyświetlania pomocy:\n"
"%s"

#: ../src/netspeed.c:808
#, c-format
msgid ""
"Failed to show:\n"
"%s"
msgstr ""
"Wyświetlenie się nie powiodło:\n"
"%s"

#: ../src/netspeed.c:839
msgid ""
"A little applet that displays some information on the traffic on the "
"specified network device"
msgstr ""
"Aplet wyświetlający informacje o ruchu na wybranym urządzeniu sieciowym."

#: ../src/netspeed.c:842
msgid "translator-credits"
msgstr ""
"Paweł Marciniak <pmarciniak@lodz.home.pl>, 2006\n"
"Julian Sikorski <belegdol@gmail.com>, 2008\n"
"Piotr Drąg <piotrdrag@gmail.com>, 2011-2012\n"
"Aviary.pl <gnomepl@aviary.pl>, 2011-2012"

#: ../src/netspeed.c:844
msgid "Netspeed Website"
msgstr "Witryna programu"

#: ../src/netspeed.c:970
msgid "Netspeed Preferences"
msgstr "Preferencje"

#: ../src/netspeed.c:993
msgid "General Settings"
msgstr "Ogólne ustawienia"

#: ../src/netspeed.c:1014
msgid "Network _device:"
msgstr "Urzą_dzenie sieciowe:"

#. Default means device with default route set
#: ../src/netspeed.c:1025
msgid "Default"
msgstr "Domyślne"

#: ../src/netspeed.c:1038
msgid "Show _sum instead of in & out"
msgstr "Wyświetlanie _sumy danych odebranych i wysłanych"

#: ../src/netspeed.c:1042
msgid "Show _bits instead of bytes"
msgstr "Wyświetlanie _bitów zamiast bajtów"

#: ../src/netspeed.c:1046
msgid "Change _icon according to the selected device"
msgstr "Zmienianie _ikony stosownie do wybranego urządzenia"

#: ../src/netspeed.c:1157
#, c-format
msgid "Device Details for %s"
msgstr "Szczegóły urządzenia %s"

#: ../src/netspeed.c:1183
msgid "_In graph color"
msgstr "Kolor wykresu danych _odebranych"

#: ../src/netspeed.c:1184
msgid "_Out graph color"
msgstr "Kolor wykresu danych _wysłanych"

#: ../src/netspeed.c:1200
msgid "Internet Address:"
msgstr "Adres internetowy:"

#: ../src/netspeed.c:1201
msgid "Netmask:"
msgstr "Maska sieci:"

#: ../src/netspeed.c:1202
msgid "Hardware Address:"
msgstr "Adres sprzętowy:"

#: ../src/netspeed.c:1203
msgid "P-t-P Address:"
msgstr "Adres P-t-P:"

#: ../src/netspeed.c:1204
msgid "Bytes in:"
msgstr "Bajtów odebranych:"

#: ../src/netspeed.c:1205
msgid "Bytes out:"
msgstr "Bajtów wysłanych:"

#: ../src/netspeed.c:1207 ../src/netspeed.c:1208 ../src/netspeed.c:1209
#: ../src/netspeed.c:1210
msgid "none"
msgstr "brak"

#: ../src/netspeed.c:1249
msgid "IPv6 Address:"
msgstr "Adres IPv6:"

#: ../src/netspeed.c:1281
msgid "Signal Strength:"
msgstr "Siła sygnału:"

#: ../src/netspeed.c:1282
msgid "ESSID:"
msgstr "ESSID:"

#: ../src/netspeed.c:1368
#, c-format
msgid "Do you want to disconnect %s now?"
msgstr "Rozłączyć urządzenie %s?"

#: ../src/netspeed.c:1372
#, c-format
msgid "Do you want to connect %s now?"
msgstr "Połączyć urządzenie %s?"

#: ../src/netspeed.c:1398
#, c-format
msgid ""
"<b>Running command %s failed</b>\n"
"%s"
msgstr ""
"<b>Wykonanie polecenia %s się nie powiodło</b>\n"
"%s"

#: ../src/netspeed.c:1454
#, c-format
msgid "%s is down"
msgstr "Połączenie sieciowe %s jest rozłączone"

#: ../src/netspeed.c:1459
#, c-format
msgid ""
"%s: %s\n"
"in: %s out: %s"
msgstr ""
"%s :%s\n"
"odebrano: %s wysłano: %s"

#: ../src/netspeed.c:1461 ../src/netspeed.c:1470
msgid "has no ip"
msgstr "nie posiada adresu IP"

#: ../src/netspeed.c:1468
#, c-format
msgid ""
"%s: %s\n"
"sum: %s"
msgstr ""
"%s: %s\n"
"w sumie: %s"

#: ../src/netspeed.c:1477
#, c-format
msgid ""
"\n"
"ESSID: %s\n"
"Strength: %d %%"
msgstr ""
"\n"
"ESSID: %s\n"
"Siła: %d %%"

#: ../src/netspeed.c:1478
msgid "unknown"
msgstr "nieznane"

#: ../src/netspeed.c:1525
msgid "Netspeed"
msgstr "Prędkość połączenia"

#: ../src/netspeed.c:1693
msgid "Device _Details"
msgstr "Szczegóły urzą_dzenia"

#: ../src/netspeed.c:1693
msgid "_Preferences..."
msgstr "_Preferencje..."

#: ../src/netspeed.c:1693
msgid "_Help"
msgstr "Pomo_c"

#: ../src/netspeed.c:1693
msgid "_About..."
msgstr "_O programie..."
